const ModerationPubsub = require('twitch-moderation-pubsub');
const _ = require('./utils');
const cName = process.env.NODE_ENV === 'development' ? 'config-dev' : 'config';

//region is that needed?
process.on('SIGINT', function () {
	// toConsole('\nGracefully shutting down from SIGINT (Ctrl-C)');
	toConsole('Shutting down...');
	// some other closing procedures go here
	process.exit();
});
//endregion

//region Create config.js if not found
try {
	require.resolve(`./${cName}.js`);
} catch (error) {
	process.stdout.write(_.getTimestamp() + ` Can't find ${cName}.js file. Creating empty template... `);
	var f = require('fs');
	var content = `module.exports = {
	token: '',
	channelName: '',
	channelId: '',
	modName: '',
	modId: '',
	debug: false
};`;
	f.writeFileSync(`./${cName}.js`, content, function (err) {
		process.stdout.write('Failed!\n'); // new line
		toConsole(err);
		process.exit();
	});

	process.stdout.write('Done!\n');
	toConsole(`Please add your information in '${cName}.js' and restart the app`);
	process.exit();
}
//endregion

const config = require(`./${cName}`);

// names to lowercase. just in case
config.channelName = config.channelName.toLowerCase();
config.modName = config.modName.toLowerCase();

//region Logger

var winston = require('winston');
require('winston-daily-rotate-file');

var transport = new(winston.transports.DailyRotateFile)({
	dirname: `./${config.channelName}`,
	filename: `#${config.channelName}.modactions.%DATE%.log`,
	datePattern: 'YYYYMMDD',
	//format: _.messageFormatter,
	json: false
});

transport.on('rotate', function(oldFilename, newFilename) {
	toConsole('Rotated to new file -> ' + newFilename);
});

const logger = winston.createLogger({
	level: 'info',
	format: winston.format.printf(_.messageFormatter),
	transports: [
		transport
	]
});

//endregion

//logger.info('Hello World!');
//return;

toConsole(`Listening to channel '${config.channelName}'`);
toConsole('ModListener is starting...');

let pubsub = new ModerationPubsub({
	token: config.token,
	topics: [config.channelId],
	mod_id: config.modId
});

pubsub.on('data.*', (data) => {
	if(data)
	{
		if(['timeout', 'untimeout', 'ban', 'unban', 'mod', 'unmod'].includes(data.type))
		{
			var d = _.formatUserAction(data);
			toConsole(d);
			logger.info(d);
		}
		else if(['clear', 'emoteonly', 'emoteonlyoff', 'followers', 'followersoff', 'r9kbeta', 'r9kbetaoff', 'slow', 'slowoff', 'subscribers', 'subscribersoff'].includes(data.type))
		{
			var e = _.formatRoomAction(data);
			toConsole(e);
			logger.info(e);
		}
		else
			toConsole('New data.type?: '  + JSON.stringify(data));
	}
});

pubsub.on('debug', (data) => {
	if (config.debug)
		toConsole(data);
});

pubsub.on('error', (data) => {
	toConsole(data);
});

pubsub.on('shard-ready', () => {
	toConsole('ModListener ready!');
});

function toConsole(msg){
	console.log(_.getTimestamp() + ' ' + msg);
}