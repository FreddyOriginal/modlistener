const moment = require('moment');

function formatName(moderatorObj) {
	return `${moderatorObj.name}[${moderatorObj.id}]`;
}

function formatDuration(seconds) {
	var d = moment.duration({
		seconds: seconds
	});
	var t = moment.utc(d.as('milliseconds')).format('HH:mm:ss');
	var days = ~~d.asDays();
	if (days > 0)
		t = days + '.' + t;

	return t;
}

function formatFollowersDuration(duration) {
	if (!Number.isInteger(duration)) {
		duration.replace(' ', '');

		var min = 0;
		var myRegexp = /\d+\D+/g;
		var match;
		/* eslint-disable-next-line no-cond-assign */
		while (match = myRegexp.exec(duration)) {
			var v = match[0];
			//var split = v.split(/(?<=\D)(?=\d)|(?<=\d)(?=\D)/);
			var split = v.split(/(\d+)(\D+)/);
			var numb = parseInt(split[1]);
			var type = split[2];
			switch (type) {
				case 'm':
				case 'minute':
				case 'minutes':
					min = min + (numb);
					break;
				case 'h':
				case 'hour':
				case 'hours':
					min = min + (numb * 60);
					break;
				case 'd':
				case 'day':
				case 'days':
					min = min + (numb * 60 * 24);
					break;
				case 'w':
				case 'week':
				case 'weeks':
					min = min + (numb * 60 * 24 * 7);
					break;
				case 'mo':
				case 'month':
				case 'months':
					min = min + (numb * 60 * 24 * 30);
					break;
			}
		}
		duration = min;
	}

	return formatDuration(duration * 60);

}

module.exports = {
	// [18:32:23]
	getTimestamp: () => {
		return `[${moment().format('HH:mm:ss')}]`;
	},

	// [18:32:23] {message}
	messageFormatter: (data) => {
		return `[${moment().format('HH:mm:ss')}]\t${data.message}`;
	},

	// freddyoriginal[37468248]	slow(00:00:23)
	// freddyoriginal[37468248]	slowoff
	formatRoomAction: (data) => {
		var msg = `${formatName(data.moderator)}\t${data.type}`;
		if (data.type === 'slow')
			msg += `(${formatDuration(data.duration)})`;
		else if (data.type === 'followers')
			msg += `(${formatFollowersDuration(data.duration)})`;
		return msg;
	},

	// xanbot[29201680]	timeout(00:00:10)	mynameislonny[75769206]	( kicked for posting link -)
	formatUserAction: (data) => {
		var msg = `${formatName(data.moderator)}\t${data.type}`;
		if (data.type === 'timeout')
			msg += `(${formatDuration(data.duration)})`;
		msg += `\t${formatName(data.target)}`;
		if (data.type === 'timeout' || data.type === 'ban')
			msg += `\t(${data.reason || ''})`;
		return msg;
	}
};