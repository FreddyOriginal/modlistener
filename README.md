### README ###

# config.js format #
    module.exports = {
        token: 'mytoken',
        channelName: 'channel',
        channelId: '123456789',
        modName: 'modname',
        modId: '987654321',
        debug: false
    };